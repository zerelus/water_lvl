/*CPP**********************************************************************
* FILENAME :        water_sensor_group.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Wrapper for handling of group of sensor channels
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    29 JUIN 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "ll_sensor_group.hpp"

//===| Definitions for SensorGroup |===========================================
//-----------------------------------------------------------------------------
SensorGroup::SensorGroup()
{
}

//-----------------------------------------------------------------------------
SensorGroup::SensorGroup(std::initializer_list<std::pair<unsigned int const, SensorChannel*> > init_channels):
    sensor_channels_(init_channels)
{

}

//-----------------------------------------------------------------------------
SensorGroup::~SensorGroup()
{
    this->sensor_channels_.clear();
}

//-----------------------------------------------------------------------------
SensorGroup::SENSOR_GROUP_STATUS SensorGroup::get_group_status()
{
    this->update_group_status();
    return this->group_status_;
}

//-----------------------------------------------------------------------------
void SensorGroup::update_group_status()
{
    bool previous_ch_value = 0, current_ch_value = 0;
    bool first_iter = true;
    this->group_status_ = SensorGroup::SENSOR_GROUP_STATUS::SENSOR_GROUP_OK;
    for(std::map<unsigned int, SensorChannel*>::iterator it = this->sensor_channels_.begin(); 
        it != this->sensor_channels_.end(); 
        it++)
    {
        if(it->second->get_channel_status() != SensorChannel::LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_OK)
        {
            this->group_status_ = SensorGroup::SENSOR_GROUP_STATUS::SENSOR_GROUP_ERROR;
            break;
        }
        else{
            current_ch_value = it->second->get_channel_output_value();
            if(first_iter)
            {
                first_iter = false;
            }
            else
            {
                if(previous_ch_value == false && current_ch_value == true)
                {
                    this->group_status_ = SensorGroup::SENSOR_GROUP_STATUS::SENSOR_GROUP_ERROR; ///-> BOTTLE is upside down
                }
            }    
            previous_ch_value = current_ch_value;
        }
    }
}

//-----------------------------------------------------------------------------
void SensorGroup::add_channel(const int sensor_value, SensorChannel *channel)
{
    this->sensor_channels_.insert(std::pair<unsigned int, SensorChannel*>(sensor_value, channel));
}

//-----------------------------------------------------------------------------
void SensorGroup::remove_channel(const int sensor_value)
{
    std::map<unsigned int, SensorChannel*>::iterator it = this->sensor_channels_.find(sensor_value);
    this->sensor_channels_.erase(it);
}

//-----------------------------------------------------------------------------
void SensorGroup::remove_channel_by_value(const SensorChannel* channel_to_remove)
{
    for(std::map<unsigned int, SensorChannel*>::iterator it = this->sensor_channels_.begin(); it != this->sensor_channels_.end(); it++)
    {
        if((it->second) == channel_to_remove)
        {
            this->sensor_channels_.erase(it);
            break;
        }
    }
}

//-----------------------------------------------------------------------------
unsigned int SensorGroup::get_channel_value(const int sensor_value)
{
    std::map<unsigned int, SensorChannel*>::iterator it = this->sensor_channels_.find(sensor_value);
    
    return it == this->sensor_channels_.end() ? -1 : (it->second)->get_channel_output_value();
}

//-----------------------------------------------------------------------------
std::map<unsigned int, unsigned int> SensorGroup::get_gr_values()
{
    std::map<unsigned int, unsigned int> value_map;

    for(const std::pair<unsigned int, SensorChannel*> &it : this->sensor_channels_)
    {
        value_map[it.first] = it.second->get_channel_output_value();
    }

    return value_map;
}

//-----------------------------------------------------------------------------
unsigned int SensorGroup::get_gr_volume()
{
    unsigned int volume = 0;
    for(auto const& pair : this->sensor_channels_)  ///-> Note: map are ordered by keys with operatior <
    {
        if(pair.second->get_channel_output_value() == 0) break;
        
        volume = pair.first;        
    }
    
    return volume;
}
//===| EOF |===================================================================