/*CPP**********************************************************************
* FILENAME :        ll_prj.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Main liquid level file to be reproduced from any platform
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    11 JUL 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "ll_prj.hpp"
#include "ll_oled_wrapper.hpp"


//===| Definitions for LLPrj |=================================================
//-----------------------------------------------------------------------------
LLPrj::LLPrj()
{
    pinMode(ERROR_PIN, OUTPUT);  //< Indicating pin
    pinMode(SW_RST_PIN, INPUT_PULLUP);  //< Reset data_handler interrupt pin
    digitalWrite(ERROR_PIN, HIGH); ///< Set to no error
    
    Serial.begin(BAUDRATE);         // Set's up Serial Communication at certain speed.

    init_vars();
    
    if(error)error_loop();
}

//-----------------------------------------------------------------------------
LLPrj::~LLPrj()
{
    delete_vars();
}

//-----------------------------------------------------------------------------
void LLPrj::start()
{
    attachInterrupt(digitalPinToInterrupt(SW_RST_PIN), LLPrj::reset_interrupt_callback_ll_prj, FALLING);
    ticker_.attach_ms(APP_PERIOD, std::bind(&LLPrj::ticker_callback_ll_prj, this)); //-> should be last step
}

//-----------------------------------------------------------------------------
void LLPrj::ticker_callback_ll_prj()
{
    scheduler->schedule(step_count);

    step_count++;

    if(error)this->error_loop();
}

//-----------------------------------------------------------------------------
void ICACHE_RAM_ATTR LLPrj::reset_interrupt_callback_ll_prj()
{
    //Debouncing
    static unsigned long last_interrupt_time = 0;
    unsigned long interrupt_time = millis();
    if (interrupt_time - last_interrupt_time > 1000)
    {
#if DEBUG_MODE == 1
        Serial.println("SW_RST Interrupt routine");
#endif
        data_handler->reset_data();
    }
    last_interrupt_time = interrupt_time;
}

//-----------------------------------------------------------------------------
void LLPrj::error_loop()
{
    while (1)
    {
        digitalWrite(ERROR_PIN, LOW);
    }
    
}