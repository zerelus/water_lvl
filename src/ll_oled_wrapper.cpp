
//===| Interface Dependencies |================================================
#include "ll_oled_wrapper.hpp"

//=============================================================================
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

//=============================================================================
void ll_oled_init()
{
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
        Serial.println(F("SSD1306 allocation failed"));
        error = 1;
    }
    display.clearDisplay();
    //display.display();
    delay(2000);
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0, 10);
    // Display static text
    display.println("Init phase finished\nCalculation begun");
    display.display();
}

//=============================================================================
void ll_oled_print_results(unsigned int time, unsigned int volume)
{
    display.clearDisplay();
    display.setCursor(0, 5);
    display.println("Process Terminated");
    display.setCursor(0, 30);
    display.printf("Calculation time: %u", time);
    display.setCursor(0, 50);
    display.printf("Volume Consumed: %u", volume);
    display.display();
}