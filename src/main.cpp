/*ARDUINO**********************************************************************
* FILENAME :        main.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Main arduino file
*       Detection of water levels
*       Project of water bottle
*       Calculate time and water level changes
*       Output water level and time 
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    22 JUIN 2020
*
*ARDUINO*/

#include "ll_oled_wrapper.hpp"
#include "main.hpp"

LLPrj ll_prj;

void setup() {
  // put your setup code here, to run once:

  ll_oled_init();

  ll_prj.start();

}

void loop() {
  if(results_ready == 1)
  {
    ll_oled_print_results(calc_time, volume);
    results_ready = 0;
  }
}
//===| EOF |===================================================================