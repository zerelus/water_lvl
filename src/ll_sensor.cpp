/*CPP**********************************************************************
* FILENAME :        water_sensor.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Method definitions for Sensor interface
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    27 JUIN 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "ll_sensor.hpp"

//===| Definitions for Sensor |================================================
//=============================================================================
/*
Sensor::Sensor():
    sensor_status_(SENSOR_STATUS::SENSOR_NOT_READY)
{
    
}*/

//=============================================================================
Sensor::Sensor(int pin_id):
    pin_id(pin_id),
    sensor_status_(SENSOR_STATUS::SENSOR_OK)
{
    pinMode(pin_id, INPUT);
}

//=============================================================================
Sensor::~Sensor()
{
}

//=============================================================================
unsigned int Sensor::get_pin_id()
{
    return this->pin_id;
}

//=============================================================================
unsigned int Sensor::acq_data()
{
    this->output_value_ = !digitalRead(this->pin_id); ///-> Inverse signal for negative logic in hardware
    return this->output_value_;
}

//=============================================================================
unsigned int Sensor::get_output_value()
{
    return this->output_value_;
}

//=============================================================================
Sensor::SENSOR_STATUS Sensor::get_sensor_status()
{
    return this->sensor_status_;
}
//===| EOF |===================================================================