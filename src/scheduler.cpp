/*CPP**********************************************************************
* FILENAME :        scheduler.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Scheduler handler for periodic_models and derived objects
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    2 JUL 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "scheduler.hpp"

//===| Definitions for Scheduler |=============================================
//=============================================================================
Scheduler::Scheduler()
{
}

//=============================================================================
Scheduler::Scheduler(std::vector<PeriodicModel*> &p_models):
    p_models_(p_models)
{
}

//=============================================================================
Scheduler::~Scheduler()
{
    this->p_models_.clear();
}

//=============================================================================
void Scheduler::schedule(unsigned long step_count)
{
    for(PeriodicModel* p_model: this->p_models_)
    {
        if((step_count % p_model->get_period()) == 0 )
        {
            p_model->operator()();
        }
    }
}

//=============================================================================
void Scheduler::add_model(PeriodicModel *p_model)
{
#if DEBUG_MODE == 1
    Serial.println("==============================================================================================");
    Serial.printf("Scheduler:: adding new periodic model\n");
#endif

    this->p_models_.push_back(p_model);

#if DEBUG_MODE == 1
    Serial.printf("\tPeriodic model number %d at Period cycle : %d\n", this->p_models_.size(), p_model->get_period());
    Serial.println("==============================================================================================");
#endif
}
//===| EOF |===================================================================