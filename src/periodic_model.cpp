/*CPP**********************************************************************
* FILENAME :        periodic_model.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Wrapper for handling of a periodic task with arduino projects
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    29 JUIN 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "periodic_model.hpp"

//===| Definitions for PeriodicModel |=========================================
//=============================================================================
unsigned int PeriodicModel::num_periodic_models = 0;

//=============================================================================
PeriodicModel::PeriodicModel(int cyclical_period):
    period_(cyclical_period)
{
    PeriodicModel::num_periodic_models++;
}

//=============================================================================
PeriodicModel::~PeriodicModel()
{
    PeriodicModel::num_periodic_models--;
}

//=============================================================================
unsigned int PeriodicModel::get_period()
{
    return this->period_;
}

//=============================================================================
unsigned int PeriodicModel::get_num_p_models()
{
    return PeriodicModel::num_periodic_models;
}

//=============================================================================
void PeriodicModel::operator()()
{   
    /*
    this->current_cycle_++;
    
    if(this->current_cycle_ == this->period_)
    {
        this->step_func();

        this->current_cycle_=0;
    }*/
    this->step_func();
    this->model_step_count_++;
}

//===| EOF |===================================================================