/*CPP**********************************************************************
* FILENAME :        handle_vars.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Variable handler file initialisation and organisation
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    3 JUL 2020
*
*CPP*/

#include "handle_vars.hpp"

int error = 0;
unsigned int step_count = 0;
SensorChannel *channel_0, *channel_1, *channel_2;
LLDataHandler *data_handler;
SensorGroup *sensor_group;
Scheduler *scheduler;
int results_ready = 0;
unsigned int calc_time = 0, volume = 0;


void init_group();
void init_channels();
void init_data_handler();

void init_vars()
{
    scheduler = new Scheduler;
    init_channels();
    init_group();
    init_data_handler();
}

void init_group()
{
    sensor_group = new SensorGroup {
        {200, (SensorChannel*)channel_0},
        {400, (SensorChannel*)channel_1},
        {600, (SensorChannel*)channel_2}
    };
}

void init_channels()
{
    int pins_channnel_0[] = {16, 13}; 
    int pins_channnel_1[] = {14, 2};
    int pins_channnel_2[] = {12, 0};

    channel_0 = new SensorChannel(0, sizeof(pins_channnel_0)/sizeof(int), pins_channnel_0, 300);
    channel_1 = new SensorChannel(1, sizeof(pins_channnel_1)/sizeof(int), pins_channnel_1, 300);
    channel_2 = new SensorChannel(2, sizeof(pins_channnel_2)/sizeof(int), pins_channnel_2, 300);

    scheduler->add_model(channel_0);
    scheduler->add_model(channel_1);
    scheduler->add_model(channel_2);
}

void init_data_handler()
{
    unsigned int data_handler_period = 100;
    data_handler = new LLDataHandler(TRACKING_PERIOD, sensor_group, data_handler_period, APP_PERIOD);
    scheduler->add_model(data_handler);
}

void delete_vars()
{
    delete scheduler;
    delete sensor_group;
    delete data_handler;
    delete channel_0;
    delete channel_1;
    delete channel_2;
}
//===| EOF |===================================================================