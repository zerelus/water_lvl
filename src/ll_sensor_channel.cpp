/*CPP**********************************************************************
* FILENAME :        water_sensor_channel.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Wrapper for handling of channel of sensors
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    27 JUIN 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "ll_sensor_channel.hpp"

//===| Definitions for LLDataHandler |=========================================
//-----------------------------------------------------------------------------
unsigned int SensorChannel::validity_timer_threshold_ = 2;

//-----------------------------------------------------------------------------
SensorChannel::SensorChannel(unsigned int channel_id, int num_sensor, int pins[], int refresh_cycle):
    PeriodicModel(refresh_cycle),
    channel_status_(LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_OK),
    channel_id_(channel_id),
    channel_output_value_(0),
    prev_channel_output_value_(0),
    validity_timer_(0),
    state_var_(1),
    sensors_(pins, pins + num_sensor)
{    
#if DEBUG_MODE == 1
    Serial.println("\n==============================================================================================");
    Serial.printf("Connecting Channel : %d, num_sensors : %d\n", this->channel_id_, this->sensors_.size());

    for(std::vector<Sensor>::iterator sensor = this->sensors_.begin(); 
        sensor != this->sensors_.end();
        sensor++
    )
    {
        Serial.printf("\t GPIO pin : %d\n", sensor->get_pin_id());
    }
    Serial.printf("Channel Connected -- Ready for use\n");
    Serial.println("==============================================================================================");
#endif
}

//-----------------------------------------------------------------------------
SensorChannel::~SensorChannel()
{
    sensors_.clear();
}

//-----------------------------------------------------------------------------
unsigned int SensorChannel::get_channel_id()
{
    return this->channel_id_;
}

//-----------------------------------------------------------------------------
unsigned int SensorChannel::get_num_sensors()
{
    return this->sensors_.size();
}

//-----------------------------------------------------------------------------
unsigned int SensorChannel::get_channel_output_value()
{
    return this->channel_output_value_;
}

//-----------------------------------------------------------------------------
unsigned int SensorChannel::get_validity_timer_threshold()
{
    return this->validity_timer_threshold_;
}

//-----------------------------------------------------------------------------
SensorChannel::LL_SENSOR_CHANNEL_STATUS SensorChannel::get_channel_status()
{
    return this->channel_status_;
}

//-----------------------------------------------------------------------------
void SensorChannel::update_channel()
{
    bool first_sensor = true;
    bool previous_pin_data = 0, current_pin_data = 0;
    bool channel_is_valid = true; ///-> channel validity is set is all sensors have the same value
    
    for(std::vector<Sensor>::iterator sensor = this->sensors_.begin(); 
        sensor != this->sensors_.end() &&  channel_is_valid == true;
        sensor++
    )
    {
        current_pin_data = sensor->acq_data();
        // Sensor validity already checked
        if(first_sensor){
            first_sensor = false;
            previous_pin_data = current_pin_data;
        }
        if(current_pin_data != previous_pin_data)
        {
            channel_is_valid = false;
            channel_status_ = LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_BUSY;
        }
        previous_pin_data = current_pin_data;
    }

    if(channel_is_valid)
    {
        this->channel_output_value_ = current_pin_data;

        switch (state_var_)
        {
        case 0 /*Balance*/:
            if(this->channel_output_value_ != this->prev_channel_output_value_)
            {
                this->prev_channel_output_value_ = this->channel_output_value_;
                this->state_var_ = 1;
                this->validity_timer_ = 0;
                this->channel_status_ = LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_BUSY;
            }
            break;
        
        case 1 /*Transition*/:
            if(this->channel_output_value_ != this->prev_channel_output_value_)
            {
                this->prev_channel_output_value_ = this->channel_output_value_;
                this->state_var_ = 1;
                this->validity_timer_ = 1;
                this->channel_status_ = LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_BUSY;
            }
            else
            {
                if(this->validity_timer_ == this->validity_timer_threshold_)
                {
                    this->state_var_ = 0; // ->balanced
                    this->channel_status_ = LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_OK;
                    this->validity_timer_ = 0;
                }
                else
                {
                    this->validity_timer_++;
                    this->channel_status_ = LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_BUSY;
                }
                
            }
        default:
            break;
        }
    }
#if DEBUG_MODE == 1
    Serial.println("==============================================================================================");
    Serial.printf("Channel %d is updated, value %d at status %d\n", this->channel_id_, this->channel_output_value_, this->channel_status_);
#endif
}

//-----------------------------------------------------------------------------
void SensorChannel::update_channel_status()
{
    Sensor::SENSOR_STATUS sensor_status = Sensor::SENSOR_STATUS::SENSOR_OK;
    for(std::vector<Sensor>::iterator sensor = this->sensors_.begin(); 
        sensor != this->sensors_.end() ||  sensor_status != Sensor::SENSOR_STATUS::SENSOR_OK;
        sensor++
    )
    {
        if(sensor->get_sensor_status() != Sensor::SENSOR_STATUS::SENSOR_OK)
        {
            sensor_status = Sensor::SENSOR_STATUS::SENSOR_ERROR;
        }
    }

    this->channel_status_ = sensor_status == Sensor::SENSOR_STATUS::SENSOR_OK ? LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_OK : LL_SENSOR_CHANNEL_STATUS::WATER_SENSOR_CHANNEL_ERROR;
}

//-----------------------------------------------------------------------------
void SensorChannel::step_func()
{
    this->update_channel_status();

    if(channel_status_ == LL_SENSOR_CHANNEL_STATUS::SENSOR_CHANNEL_OK)
    {
        this->update_channel();
    }
}
//===| EOF |===================================================================