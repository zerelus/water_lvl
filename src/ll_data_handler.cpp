/*CPP**********************************************************************
* FILENAME :        ll_data_handler.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Data handler to process data from LLSensorGroup
*       Derived of PeriodicModel
*       To be executed periodically
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    4 JUL 2020
*
*CPP*/

//===| Interface Dependencies |================================================
#include "ll_data_handler.hpp"

//===| Definitions for LLDataHandler |=========================================
//-----------------------------------------------------------------------------
LLDataHandler::LLDataHandler(unsigned long tracking_period, SensorGroup *sensor_group, unsigned int refresh_cycle, unsigned int app_period):
    PeriodicModel(refresh_cycle),
    sensor_group_(sensor_group),
    sensor_gr_calibrated_(false),
    liq_volume_(0),
    app_period_(app_period)
{
    this->tracking_period_ = tracking_period / (app_period * refresh_cycle);
    this->calibrate_sensor_gr();

    //ll_oled_init();
}

//-----------------------------------------------------------------------------
LLDataHandler::~LLDataHandler()
{
}

//-----------------------------------------------------------------------------
void LLDataHandler::calibrate_sensor_gr()
{
    if(this->sensor_group_->get_group_status() == SensorGroup::SENSOR_GROUP_STATUS::SENSOR_GROUP_OK)
    {
        this->current_ch_volume_ = this->sensor_group_->get_gr_volume();
        this->previous_ch_volume_ = this->current_ch_volume_;
        this->sensor_gr_calibrated_ = true;
    }
}

//-----------------------------------------------------------------------------
void LLDataHandler::reset_data()
{
    this->reset_volume();
    this->model_step_count_ = 0;
}

//-----------------------------------------------------------------------------
void LLDataHandler::reset_volume()
{
    this->liq_volume_ = 0;
    this->sensor_gr_calibrated_ = false;
    this->calibrate_sensor_gr();
}

//-----------------------------------------------------------------------------
void LLDataHandler::step_func()
{
    if(!this->sensor_gr_calibrated_)
    {
        this->calibrate_sensor_gr();
    }
    if(this->sensor_gr_calibrated_)
    {
        if((this->model_step_count_ != 0) && ((this->model_step_count_ % this->tracking_period_) == 0))
        {
            this->print_result();
            this->reset_volume();
        }
        else
        {
            this->process_group();
        }
    }
}

//-----------------------------------------------------------------------------
void LLDataHandler::process_group()
{
    if(this->sensor_group_->get_group_status() == SensorGroup::SENSOR_GROUP_STATUS::SENSOR_GROUP_OK)
    {
        this->current_ch_volume_ = this->sensor_group_->get_gr_volume();
        if(this->current_ch_volume_ < this->previous_ch_volume_)
        {
            this->liq_volume_ += (this->previous_ch_volume_ - this->current_ch_volume_);
            
#if DEBUG_MODE == 1
            Serial.println("\n==============================================================================================");
            Serial.printf("LLDataHandler :: changed liquid volume\n\tLiquid consumed %u ml\n", this->liq_volume_);
#endif
        }
        this->previous_ch_volume_ = this->current_ch_volume_;
    }
}

//-----------------------------------------------------------------------------
void LLDataHandler::print_result()
{
#if DEBUG_MODE == 1
    Serial.println("\n==============================================================================================");
    Serial.println("Terminated calculation period with following results:");
    Serial.printf("\tPeriod of analysis: %lu\n\tVolume of liquid consumed : %u\n", this->tracking_period_, this->liq_volume_);
    Serial.println("\n==============================================================================================");
#endif
    calc_time = this->tracking_period_;
    volume = this->liq_volume_;
    results_ready = 1;
}
//===| EOF |===================================================================