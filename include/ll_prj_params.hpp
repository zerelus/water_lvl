/*H**********************************************************************
* FILENAME :        water_lvl_params.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Definition file for application specific parameters
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    30 JUIN 2020
*
*H*/

#ifndef LL_PARAMS_H
#define LL_PARAMS_H

#ifdef __cplusplus
extern "C" {
#endif

#define DEBUG_MODE 1
#define ERROR_PIN 3
#define SW_RST_PIN 15
#define BAUDRATE 115200


#define APP_PERIOD 10

#define TRACKING_PERIOD 60000//86400000 //->milliseconds

#define NUM_SENSORS 6
#define NUM_CHANNELS 3

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* LL_PARAMS_H */
//===| EOF |===================================================================