/*H**********************************************************************
* FILENAME :        ll_data_handler.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       OLED I2C 128x64 screen wrapper for ll_prj specifice
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    23 JUL 2020
*
*H*/

//===| Interface Dependencies |================================================
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET    -1 // Reset pin # (or -1 if sharing Arduino reset pin)

//=============================================================================
extern int error;

//=============================================================================
void ll_oled_init();

//=============================================================================
void ll_oled_print_results(unsigned int time, unsigned int volume);