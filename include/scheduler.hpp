/*H**********************************************************************
* FILENAME :        scheduler.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Scheduler handler for periodic_models and derived objects
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    2 JUL 2020
*
*H*/

#ifndef SCHEDULER_H
#define SCHEDULER_H

//===| Interface Dependencies |================================================
#include "Arduino.h" 
#include <vector>
#include "periodic_model.hpp"
#include "ll_prj_params.hpp"

//=============================================================================
/**
 * Class Scheduler wrapper for periodic_models
 * Real Time application to call schedule() method and execute tasks
 */
class Scheduler
{
public:
    /**
     * A constructor
     * Default
     */ 
    Scheduler();

    /**
     * A constructor
     * Initialized PeriodicModels* vector
     */
    Scheduler(std::vector<PeriodicModel*> &p_models);

    /**
     * A destructor
     * Models cleared
     */
    ~Scheduler();

    /**
     * Add periodic model
     * @param p_model pointer to task to be added
     */ 
    void add_model(PeriodicModel *p_model);
    
    /**
     * Schedule method
     * To be called at application period
     * Execute registered tasks
     * @param step_count step count of main application 
     */
    void schedule(unsigned long step_count);

private:
    std::vector<PeriodicModel*> p_models_;  ///< models used by polymorphism
};

#endif /* SCHEDULER_H */
//===| EOF |===================================================================