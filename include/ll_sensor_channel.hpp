/*H**********************************************************************
* FILENAME :        water_sensor_channel.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Wrapper for handling of channel of sensors
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    27 JUIN 2020
*
*H*/

#ifndef LL_SENSOR_CHANNEL_H
#define LL_SENSOR_CHANNEL_H

//===| Interface Dependencies |================================================
#include "Arduino.h"
#include <vector>
#include "ll_sensor.hpp"
#include "periodic_model.hpp"
#include "ll_prj_params.hpp"

//=============================================================================
/**
 * Class handler for sensor of same volume level
 * Sensors of same channel must have the same value to be stable output
 * Time of stability for registered output is determined by validity_threshhold_ member
 * Periodic Class derived from PeriodicModel
 */
class SensorChannel: public PeriodicModel
{
public:
    /**
     * Enum Type
     * SensorChannel status options
     */
    enum LL_SENSOR_CHANNEL_STATUS{SENSOR_CHANNEL_OK, WATER_SENSOR_CHANNEL_ERROR, SENSOR_CHANNEL_BUSY}; ///-> Error handling for SensorChannel

    /**
     * A constructor
     * @param channel_id Channel; id
     * @param num_sensor number of sensor being assigned to channel
     * @param pins array of pins that sensors are connected to hardware, must be same size as num_sensors
     * @param refresh_cycle period of step_func()
     */
    SensorChannel(unsigned int channel_id, int num_sensor, int pins[], int refresh_cycle);

    /**
     * A destructor
     * Clear sensors
     */ 
    ~SensorChannel();

    /**
     * Getter method channel_id_
     * @return channel_id_
     */ 
    unsigned int get_channel_id();

    /**
     * Method get number of sensors connected to channel
     * @return number of sensors
     */
    unsigned int get_num_sensors();

    /**
     * Get output_value of channel. 
     * Recommended to verify channel validity with get_channel_status() method first
     * @return output value of channel
     */ 
    unsigned int get_channel_output_value();

    /**
     * Get validity_threshold_.
     * Time required for all sensor to be stabilized and register output_value
     * @return validity_timer_
     */
    unsigned int get_validity_timer_threshold();

    /**
     * Getter for channel_status_
     * @return channel status
     * @sa LL_SENSOR_CHANNEL_STATUS
     */
    LL_SENSOR_CHANNEL_STATUS get_channel_status();

    /**
     * Updates output value of channel by updating and verifying output_value of the sensors
     */ 
    void update_channel();

    /**
     * Update channel_status by checking status of sensors
     */
    void update_channel_status();

    /**
     * Override methode for periodic call at specified refresh_cycle in constructor
     * @sa SensorChannel(unsigned int channel_id, int num_sensor, int pins[], int refresh_cycle)
     */
    virtual void step_func();
    
private:
    /* data */
    LL_SENSOR_CHANNEL_STATUS channel_status_;

    unsigned int channel_id_;   ///< Not really useful maybe remove
    unsigned int channel_output_value_; ///< Balanced sensor value stored in this member
    unsigned prev_channel_output_value_;    ///< previous value needs to be recorded for state changes
    unsigned int validity_timer_;   ///< member used to count up to validoity_timer_threshold
    unsigned int state_var_;    ///< State member for state machine used in update_channel() method
    std::vector<Sensor> sensors_;   ///< container for 

    static unsigned int validity_timer_threshold_;  ///< validity_timer_threshold_ is set to static, same for all channels (maybe shoudl be variable)
};

#endif /*LL_SENSOR_CHANNEL_H*/
//===| EOF |===================================================================