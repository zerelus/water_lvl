/*H**********************************************************************
* FILENAME :        periodic_model.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Wrapper for handling of a periodic task with arduino projects
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    29 JUIN 2020
*
*H*/

#ifndef PERIODIC_MODEL_H
#define PERIODIC_MODEL_H

//===| Interface Dependencies |================================================

//=============================================================================
/**
 * Abstract class to handle periodic tasks
 * Operator() to be called at app specific step
 * Application is responsible for cycle_ management
 */
class PeriodicModel
{
public:
    /**
     * A constructor
     * @param refresh_cycle cycle of execution depending of app period
     */
    PeriodicModel(int refresh_cycle);

    /**
     * A destructor
     * Empty
     */
    virtual ~PeriodicModel();

    /**
     * Getter method period_
     * @return model period
     */ 
    unsigned int get_period();

    /**
     * Getter 
     * Static method
     * @return number of periodic models
     */
    static unsigned int get_num_p_models();

    /**
     * Operator() interface with application to call model step_func()
     */
    void operator() ();

    /**
     * Virtual abstract method to be implemented by child class
     * Method to do required task periodically
     */
    virtual void step_func() = 0;

protected:
    static unsigned int num_periodic_models;    ///< static to keep track
    unsigned int period_;   ///< period is factor of app specific period
    unsigned long model_step_count_;    ///< number of step finished 

private:
    /* data */    
    
};

#endif /* PERIODIC_MODEL_H */
//===| EOF |===================================================================