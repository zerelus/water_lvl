/*H**********************************************************************
* FILENAME :        water_sensor.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Class to controle water sensor inteface 
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    27 JUIN 2020
*
*H*/

#ifndef LL_SENSOR_H
#define LL_SENSOR_H

//===| Interface Dependencies |================================================
#include <Arduino.h>

//=============================================================================
/**
 * Class used to interface with sensor at harware level
 * Not periodic
 * Purposed to be utilised by another process to be interfaced
 * Todo:: Verify sensor validity. but how?
 */
class Sensor
{
public:
    /**
     * Enum Type
     * Senosr status options
     */
    enum SENSOR_STATUS{SENSOR_OK, SENSOR_ERROR, SENSOR_BUSY, SENSOR_NOT_READY}; ///-> Error handling for Sensor

    //Sensor();

    /**
     * A constructor
     * @param pin_id gpio_pin at hardware level connected to liquid level senor, will be set as INPUT
     */
    Sensor(int pin_id);

    /**
     * A destructor 
     * Empty
     */
    ~Sensor();

    /**
     * Getter pin_id_
     * @return gpio pin sensor lecture
     */
    unsigned int get_pin_id();

    /**
     * Read value from pin and update value
     * @return sensor output signal
     */
    unsigned int acq_data();

    /**
     * Getter output_value_
     * @return sensor output signal
     */
    unsigned int get_output_value();

    /**
     * Getter sensor_status
     * @return sensor_status_
     * @sa SENSOR_STATUS
     */ 
    SENSOR_STATUS get_sensor_status();    

private:
    /* data */
    unsigned int pin_id;    ///< gpio pin at hardware level
    SENSOR_STATUS sensor_status_;
    unsigned int output_value_; ///< registered output_signal from signal

};

#endif /* LL_SENSOR_H */
//===| EOF |===================================================================