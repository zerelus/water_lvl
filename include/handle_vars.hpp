/*H**********************************************************************
* FILENAME :        handle_vars.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Variable handler file initialisation and organisation
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    3 JUL 2020
*
*H*/

#ifndef HANDLE_VARS_H
#define HANDLE_VARS_H

#include "ll_prj_params.hpp"
#include "periodic_model.hpp"
#include "ll_sensor_channel.hpp"
#include "ll_data_handler.hpp"
#include "ll_sensor_group.hpp"
#include "scheduler.hpp"

extern int error; // Error variable 
extern unsigned int step_count;

extern SensorChannel *channel_0, *channel_1, *channel_2;
extern LLDataHandler *data_handler;
extern Scheduler *scheduler;

extern int results_ready;
extern unsigned int calc_time, volume;

void init_vars();
void delete_vars();

#endif /* HANDLE_VARS_H */
//===| EOF |===================================================================