/*H**********************************************************************
* FILENAME :        ll_data_handler.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Data handler to process data from LLSensorGroup
*       Derived of PeriodicModel
*       To be executed periodically
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    4 JUL 2020
*
*H*/

#ifndef LL_DATA_HANDLER_H
#define LL_DATA_HANDLER_H

//===| Interface Dependencies |================================================
#include "periodic_model.hpp"
#include "ll_sensor_group.hpp"
#include "ll_prj_params.hpp"
#include "ll_oled_wrapper.hpp"

extern int results_ready;
extern unsigned int calc_time, volume;

//=============================================================================
/**
 *  Class to handle and process data providing from a SensorGroup
 *  Class is periodic and needs real_time knowledge for printing purposes
 */
class LLDataHandler: public PeriodicModel
{
public:
    /**
     * Constructor
     * @param tracking_period Period in millisecond to track liquid level
     * @param sensor_group pointer to already initialized SensorGroup
     * @param refresh_cycle period for step_func()
     * @param app_period real-time period for application
     */
    LLDataHandler(unsigned long tracking_period, SensorGroup *sensor_group_, unsigned int refresh_cycle, unsigned int app_period);

    /**
     * Destructor: Empty
     */
    ~LLDataHandler();

    /**
     * Override step_func() for periodic process
     */
    virtual void step_func();

    /**
     * Resets data to calibrate with group for tracking
     */ 
    void calibrate_sensor_gr();

    /**
     * Reset LLDataHandler object to initial state and restart calculation
     */ 
    void reset_data();

    /**
     * Reset volume to 0 and resume calculation
     */
    void reset_volume();

    /**
     * Printing method for data 
     */
    void print_result();

    /**
     * Process group data for tracking volume member
     */
    void process_group();

private:
    /* data */
    SensorGroup *sensor_group_;
    bool sensor_gr_calibrated_;  ///< Calibration validation member
    unsigned long tracking_period_; 
    unsigned int liq_volume_;
    unsigned int previous_ch_volume_, current_ch_volume_;   ///< Previous and current liquid_volume 
    unsigned int app_period_;
};
#endif /* LL_DATA_HANDLER_H */
//===| EOF |===================================================================