/*H**********************************************************************
* FILENAME :        ll_prj.cpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Main liquid level file to be reproduced from any platform
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    11 JUL 2020
*
*H*/

#ifndef LL_PRJ_H
#define LL_PRJ_H

//===| Interface Dependencies |================================================
#include <Ticker.h>
#include <functional>

#include "ll_prj_params.hpp"
#include "scheduler.hpp"
#include "handle_vars.hpp"

//=============================================================================
class LLPrj
{

public:
    /**
     * A constructor
     * Setup project and variable
     */
    LLPrj();

    /**
     * A destructor
     * Clean up
     */
    ~LLPrj();

    /**
     * Start method to begin periodic ticker
     */
    void start();

    /**
     * ticker callback application body
     */
    void ticker_callback_ll_prj();

    /**
     * reset callback, only reset calculation period
     */
    static void ICACHE_RAM_ATTR reset_interrupt_callback_ll_prj();
    
    /**
     * Error blocking function 
     * Need to reset arduino manually to retry
     * Or may require further debugging of issue
     */
    static void error_loop();

private:
    Ticker ticker_; // Ticker Variable

};

#endif /* LL_PRJ_H */
//===| EOF |===================================================================