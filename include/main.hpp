/*H**********************************************************************
* FILENAME :        main.hpp             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Main arduino file
*       Detection of water levels
*       Project of water bottle
*       Calculate time and water level changes
*       Output water level and time 
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    30 JUIN 2020
*
*H*/

#ifndef MAIN_H
#define MAIN_H

//===| Interface Dependencies |================================================
#include <Arduino.h>
#include "ll_prj.hpp"
#include "handle_vars.hpp"


#endif /* MAIN_H */
//===| EOF |===================================================================