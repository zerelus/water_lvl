/*H**********************************************************************
* FILENAME :        water_sensor_group.h             DESIGN REF: 
*
* DESCRIPTION :
*       File main
*
* PUBLIC FUNCTIONS :
*       
*
* NOTES :
*       Wrapper for handling of group of sensor channels
*
*
* AUTHOR :    Saba Ebrahiminia        START DATE :    29 JUIN 2020
*
*H*/

#ifndef WATER_SENSOR_GROUP_H
#define WATER_SENSOR_GROUP_H

//===| Interface Dependencies |================================================
#include <Arduino.h>
#include <map>
#include <initializer_list>
#include "ll_sensor_channel.hpp"

//=============================================================================
/**
 * Sensor group class to handle group of channels
 * Handling status, liquid level and channel validity
 * Channels are oragnised by std::map<unsigned int, SensorChannel*> where key is liquid volume
 */ 
class SensorGroup
{
public:
    /**
     * Enum Type
     * SensorGroup status options
     */
    enum SENSOR_GROUP_STATUS{SENSOR_GROUP_OK, SENSOR_GROUP_ERROR}; ///< Error handling for Sensor 

    /**
     * A constructor
     * Default
     */ 
    SensorGroup();

    /**
     * A constructor
     * Constructor by initialize_list 
     * For ex: new SensorGroup{channels}
     */ 
    SensorGroup(std::initializer_list<std::pair<unsigned int const, SensorChannel*> > init_channels);

    /**
     * A destructor
     * Clear channels
     */ 
    ~SensorGroup();

    /**
     * Getter method : sensor_status
     */
    SENSOR_GROUP_STATUS get_group_status();

    /**
     * Update group_status by checking on channels
     */
    void update_group_status();

    /**
     * Add channel method
     * @param sensor_value liquid volume of channel to be added
     * @param channel pointer to channel to add
     */ 
    void add_channel(const int sensor_value, SensorChannel *channel);

    /**
     * Remove channel by key
     * @param sensor_value liquid volume of channel to remove
     */ 
    void remove_channel(const int sensor_value);

    /**
     * Remove channel by value
     * @param channel_to_remove pointer to channel to remove
     */
    void remove_channel_by_value(const SensorChannel* channel_to_remove);

    /**
     * Get output_value of specified channel value
     * @param sensor_value value of sensor to get output_value
     */
    unsigned int get_channel_value(const int sensor_value);

    /**
     * Retrieve all values of group in a mapping format map<key:liquid volume, value:channel output>
     * @return channel volumes and output values by map<unsigned int, unsigned int>
     */
    std::map<unsigned int, unsigned int> get_gr_values();

    /**
     * Method get current group volume
     * @return volume of sensor group
     */
    unsigned int get_gr_volume();

private:
    /* data */
    SENSOR_GROUP_STATUS group_status_;  ///< Group status member
    std::map<unsigned int, SensorChannel*> sensor_channels_;   ///< Map of sensor channel, key is volume point of channel
};

#endif /* WATER_SENSOR_GROUP_H */
//===| EOF |===================================================================